#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
	
FILE *data;
struct kontakt* K_head = NULL;
struct kontakt* K_shtese;
struct kontakt* K_aktual;

struct email* E_koka = NULL;
struct email* E_shtese;
struct email* E_aktual;

struct nr_kontakti* N_koka = NULL;
struct nr_kontakti* N_shtese;
struct nr_kontakti* N_aktual;

struct email* shtoEmail (struct email*  E_koka, char email[]);
struct nr_kontakti* shtoNrKontakti (struct nr_kontakti* N_koka, char numri[]);
struct kontakt* shtoKontakt (struct kontakt* K_head, char emri[], char mbiemri[], struct email* email, struct nr_kontakti* numri, int i);
void afishoEmail (struct email* E_koka) ;
void afishoNrKontakti (struct nr_kontakti* N_koka);
void afishoKontaktet (struct kontakt* K_head);
int isMail (char word[]);
int isPhoneNumber (char word[]);
void importoKontakte(FILE *data);
void kryejNdryshimet (struct kontakt* K_head);
void kerkoKontakt (struct kontakt* K_head, char kerko[]);
struct kontakt* fshiKontakt (struct kontakt* K_head, int x[]);
void redaktoKontakt (struct kontakt* K_head, int x);

struct email 
{
	char email[30];
	struct email* next;
};

struct nr_kontakti 
{
	char numer_kontakti[30];
	struct nr_kontakti* next;
};

struct kontakt 
{
	int numri_ID, numri_kerkimeve;
	char emri[30], mbiemri[30];
	struct email* email;
	struct nr_kontakti* numer_kontakti;
	struct kontakt* next;
};

void   printoPlq          (struct kontakt* p, int i) {
	if (p == NULL || i==10)
		return;
	printf("\n%d - %s %s\n", p->numri_ID, p->emri, p->mbiemri);
	printf("\nKerkuar: %d  here\t\t\t\t\t\t\t     %c\n", p->numri_kerkimeve, 179);

	int l;
for(l=0;l<70;l++){
			printf("%c",205);
			}
	printoPlq (p->next, i+1);
}


void   renditPlq          (struct kontakt* p) {
	struct kontakt* q;
	struct kontakt* min;
	struct kontakt *head = (struct kontakt*) malloc (sizeof (struct kontakt));
	head->next = K_head;
	p = head;

	while (p->next->next!=NULL)
	{
		min = p;
		q = p->next;
		while (q->next!=NULL)
		{
			if (min->next->numri_kerkimeve < q->next->numri_kerkimeve)
				min = q;
			q = q->next;
		}
		struct kontakt* a = p->next;
		p->next = min->next;
		min->next = a;
		struct kontakt* b = a->next;
		a->next = p->next->next;
		p->next->next = b;
		p = p->next;
	}
	K_head = head->next;
}
void   rendit             (struct kontakt* p) {
	struct kontakt* q;
	struct kontakt* min;
	struct kontakt *head = (struct kontakt*) malloc (sizeof (struct kontakt));
	head->next = K_head;
	p = head;

	while (p->next->next!=NULL)
	{
		min = p;
		q = p->next;
		while (q->next!=NULL)
		{
			if (strcmp(min->next->emri, q->next->emri)==1|| (strcmp(min->next->emri, q->next->emri)==0 && strcmp(min->next->mbiemri, q->next->mbiemri)==1))
				min = q;
			q = q->next;
		}
		struct kontakt* a = p->next;
		p->next = min->next;
		min->next = a;
		struct kontakt* b = a->next;
		a->next = p->next->next;
		p->next->next = b;
		p = p->next;
	}
	K_head = head->next;
}
int main() {
	char search[20];
	char a[10], b[10];
	int ndr, vfshi[10], i=0, e, plq[10];
	struct kontakt* p = K_head;
	data=fopen("dokument.txt", "r");
	
	importoKontakte(data);
	rendit(K_head);		

	char ch;
    do
    {
		fflush(stdin);
       int l;
		printf("%c",213);
		for(l=0;l<70;l++){
			printf("%c",205);
		}
		printf("%c", 184);
		printf("\n%c", 179);
		printf("\t\t     Universiteti Politeknik i Tiranes\t\t       %c", 179);
		printf("\n%c______________________________________________________________________%c",179, 179);
        printf("\n%c\t\t\tSHFLETUESI I KONTAKTEVE",179);
        printf("\t\t\t       %c", 179);
		printf("\n%c______________________________________________________________________%c\n",179, 179);
        printf("%c\t\t\t1 - Listo kontaktet\t\t\t       %c", 179);
	    printf("\n%c\t\t\t2 - Kerko kontaktet\t\t\t       %c",179);
	    printf("\n%c\t\t\t3 - Shto nje kontakt te ri\t\t       %c",179);
	    printf("\n%c\t\t\t4 - Redakto nje kontakt\t\t\t       %c",179);
	    printf("\n%c\t\t\t5 - Fshi kontakte\t\t\t       %c",179);
	    printf("\n%c\t\t\t6 - Listo kontaktet e preferuara\t       %c",179);
	    printf("\n%c\t\t\t7 - Info\t\t\t\t       %c",179);
	    printf("\n%c\t\t\tL - Dil\t\t\t\t\t       %c",179);
	    printf("\n%c", 192);
	    for(l=0;l<70;l++){
	    	printf("%c", 196);
	    }
	    printf("%c", 217);
        printf("\n\t\tJep Komanden: ");           
        ch = getchar();
	    ch = toupper(ch);
        switch(ch)    
        {
            case '1':
            {
	            puts("\n\t\t Lista e kontakteve ...\n");
	            afishoKontaktet(K_head);
	            break;           	
            }
                
            case '2':
            {
	            puts("Kerko kontaktet\n");
				printf("Kerko: ");
				scanf("%s", search);
				printf("\n\n");
				kerkoKontakt (K_head, search);
	            break;           	
            }
            
            case '3':
            {
	            puts("Shto nje kontakt te ri\n");
				K_shtese = (struct kontakt*) malloc (sizeof (struct kontakt));
				K_aktual = K_head;
				while (K_aktual->next!=NULL)
					K_aktual = K_aktual->next;
				K_shtese->numri_ID = K_aktual->numri_ID + 1;
				K_aktual->next = K_shtese;
				K_shtese->next = NULL;
				printf("Emri: ");
				scanf("%s", K_shtese->emri);
				printf("Mbiemri: ");
				scanf("%s", K_shtese->mbiemri);
				K_shtese->email = NULL;
				K_shtese->numer_kontakti = NULL;
				
				printf("\t\tDeshironi te shtoni adrese email ? (P (po) / J (jo)) : ");
				scanf("%s", &ch);
				ch = toupper(ch);		
				
			    while ( ch != 'J')
			    {
					 printf("\tEmail: "); scanf("%s", search);
					 K_shtese->email = shtoEmail(K_shtese->email, search);   	 

			   		 printf("\t\tDeshironi te shtoni adrese email tjeter? (P (po)/ J (jo)) : ");
				   	 scanf("%s", &ch);
				   	 ch = toupper(ch);	 	   	 
				   	 
				}
								
				printf("\t\tDeshironi te shtoni numer kontakti ? ( P (po) / J (jo) ) : ");
				scanf("%s", &ch);
				ch = toupper(ch);		
				
			    while ( ch != 'J')
			    {
					 printf("\tNumer kontakti: "); scanf("%s", search);
					 K_shtese->numer_kontakti = shtoNrKontakti (K_shtese->numer_kontakti, search);  	 

			   		 printf("\t\tDeshironi te shtoni numer tjeter kontakti? (P (po)/ J(jo)) : ");
				   	 scanf("%s", &ch);
				   	 ch = toupper(ch);	 	   	 
				   	 
				}
				
				rendit(K_head);
	            break;           	
            }
            
            case '4':
            {
	            puts("Redaktoni nje kontakt\n");
				printf("Ndryshoni: ");
				scanf("%d", &ndr);
				redaktoKontakt (K_head, ndr);
				rendit(K_head);
	            break;           	
            }
            
            case '5':
            {
	             puts("Fshini kontakte\n");
				for (i=0;i<10;i++) vfshi[i] = 0; 
				i=0;
				while (ch != 'J') 
				{
					printf("Kontakti qe doni te fshish : "); scanf("%d", &vfshi[i]);
					i++;
					
					printf("Doni te fshish kontakte te tjera ? (P (po)/ J (jo) ) : ");
				   	scanf("%s", &ch);
				   	ch = toupper(ch);	 	  									
				}

				i=0;
				K_head = fshiKontakt(K_head, vfshi);
	            break;			         	
            }
            
            case '6':
            {
	            puts("Listo kontaktet e preferuara\n");
				renditPlq(K_head);
				printoPlq(K_head, 0);
				rendit(K_head);
	            break;           	
            }
            
            case '7':
            {
	            puts("\n\n--------------------------------------------\nPunoi:\n\tTheodhor Pandeli\n\tLenda:\tAlgoritmike dhe Programim i Avancuar\n\tDega:\tInxhinieri Informatike\n\tUNIVERSITETI POLITEKNIK I TIRANES\n--------------------------------------------\n\n\n");
				
	            break;           	
            }
            
                
			            
            
        }
    }
    while(ch != 'L');	
	return 1;	
}

/*FUNKSIONET*/

int isMail (char word[])
{
	for (int i=0; i<strlen(word); i++)
	{
		if(word[i] == '@')
		return 1;
	}
			
	return 0;
}


int isPhoneNumber (char word[])
{
	for (int i=0; i<strlen(word); i++)
	{
		if(isdigit(word[i]) || word[i] == '+')
		return 1;
	}
			
	return 0;
}

struct email* shtoEmail (struct email*  E_koka, char email[]) 
{
	E_shtese = (struct email*) malloc (sizeof (struct email));
	strcpy(E_shtese->email, email);
	E_shtese->next = NULL;
	if (E_koka == NULL)
		E_koka = E_shtese;
	else
	{
		E_aktual = E_koka;
		while (E_aktual->next != NULL) E_aktual = E_aktual->next;
		E_aktual->next = E_shtese;
	}
	return E_koka;
}

struct nr_kontakti* shtoNrKontakti (struct nr_kontakti* N_koka, char numri[]) 
{
	N_shtese = (struct nr_kontakti*) malloc (sizeof (struct nr_kontakti));
	strcpy(N_shtese->numer_kontakti, numri);
	N_shtese->next = NULL;
	if (N_koka == NULL)
		N_koka = N_shtese;
	else
	{
		N_aktual = N_koka;
		while (N_aktual->next != NULL) N_aktual = N_aktual->next;
		N_aktual->next = N_shtese;
	}
	return N_koka;
}

struct kontakt* shtoKontakt (struct kontakt* K_head, char emri[], char mbiemri[], struct email* email, struct nr_kontakti* numri, int i) 
{
	K_shtese = (struct kontakt*) malloc (sizeof (struct kontakt));
	strcpy(K_shtese->emri, emri);
	strcpy(K_shtese->mbiemri, mbiemri);
	K_shtese->email = email;
	K_shtese->numer_kontakti = numri;
	K_shtese->numri_ID = i;
	K_shtese->numri_kerkimeve = 0;
	K_shtese->next = NULL;
	if (K_head == NULL)
		K_head = K_shtese;
	else {
		K_aktual = K_head;
		while (K_aktual->next != NULL)
			K_aktual = K_aktual->next; K_aktual->next = K_shtese;
	}
	return K_head;
}

void afishoEmail (struct email* E_koka) 
{
    if(E_koka==NULL)
        printf("---Ska email!---\t\t\t\t\t       %c",179);
    else
    {      
        do
        {
        	printf("%s \t\t\t\t\t       %c", E_koka->email,179);
        }
        while((E_koka = E_koka->next) != NULL);
    }	
}

void afishoNrKontakti (struct nr_kontakti* N_koka) 
{
    if(N_koka==NULL)
        printf("---Nuk ka numer!---"); 
    else
    {      
        do
        {
        	printf("%s ", N_koka->numer_kontakti);
        }
        while((N_koka = N_koka->next) != NULL);
    }		
}

void afishoKontaktet (struct kontakt* K_head) 
{
    if(K_head==NULL)
        printf("---Nuk ka kontakt!---\t\t\t\t\t%c",179); 
    else
    {      
        do
        {
        	int l;
		printf("%c",213);
		for(l=0;l<70;l++){
			printf("%c",205);
		}	printf("%c",184);
			printf("\n",179);
			printf("%c %d - %s %s					               %c\n",179, K_head->numri_ID, K_head->emri, K_head->mbiemri,179);
			printf("%c		        				\t       %c\n",179,179);
			printf("%c    ->E-mail: ",179); afishoEmail(K_head->email);
			
			printf("\n%c    ->Numer kontakti: ",179); afishoNrKontakti(K_head->numer_kontakti);
			printf("\n%c\n",179);
        }
        while((K_head = K_head->next) != NULL);
    }	
}

void importoKontakte (FILE *data) 
{
	char emri[30], mbiemri[30], email[30], numri[30];
	int i=1;
		
	if ((data=fopen("dokument.txt", "r"))==NULL) 
	{
		data=fopen("dokument.txt", "w");
		fclose(data);
	
		printf("\n\t\tKUJDES!\n\tNuk u gjend skedari dokument.txt. \n\tProgrami do te vazhdoje pa importuar kontakte ...");
	}
		
	while (!feof(data)) 
	{
		fscanf(data, "%s %s", emri, mbiemri);
		
		fscanf(data, "%s", email);
		while (strcmp(email, ",")!=0) {
			E_koka = shtoEmail(E_koka, email);
			fscanf(data, "%s", email);				
		} 
		
		fscanf(data, "%s", numri);
		while (strcmp(numri, ";")!=0) {
			N_koka = shtoNrKontakti(N_koka, numri);
			fscanf(data, "%s", numri);
		}
		
		K_head = shtoKontakt(K_head, emri, mbiemri, E_koka, N_koka, i++);
		E_koka=NULL; N_koka=NULL;
	}
	fclose(data);
}

void kryejNdryshimet (struct kontakt* K_head) 
{
	data=fopen("dokument.txt", "w");
	struct email* h1; struct nr_kontakti* h2;
	
	while (K_head!=NULL) {
		E_koka = K_head->email;
		while (E_koka!=NULL) 
		{
			fprintf(data, "%s ", E_koka->email);
			E_koka=E_koka->next;
		}
		fprintf(data, ", ");
		N_koka = K_head->numer_kontakti;
		while (N_koka!=NULL) 
		{
			fprintf(data, "%s ", N_koka->numer_kontakti);
			N_koka=N_koka->next;
		}
		fprintf(data, ";");
		K_head = K_head->next;
		if (K_head!=NULL)
			fprintf(data, "\n");
	}
	fclose(data);
}

void kerkoKontakt (struct kontakt* K_head, char kerko[]) 
{
	int i=1;
	while (K_head!=NULL) {
		if (strstr(K_head->emri, kerko)!=NULL || strstr(K_head->mbiemri, kerko)!=NULL) {
			K_head->numri_kerkimeve++;
			
			printf("%d - %s %s\n", i++, K_head->emri, K_head->mbiemri);
			printf("    E-mail: "); afishoEmail(K_head->email);
			printf("\n    Numer kontakti: "); afishoNrKontakti(K_head->numer_kontakti);
			printf("\n\n");			
		}
		K_head = K_head->next;
	}
}

void fshiKontakt1 (int x[]) 
{
	int i=0;
	while(x[i] != '\0') 
	{
		printf("%d\n", x[i]);
		i++;
	}
}

struct kontakt* fshiKontakt (struct kontakt* K_koka, int x[]) 
{
    int i =0;
	if (!K_koka)
    { 
        return K_koka;
    }
    
	while (x[i]!='\0') 
	{    
		struct kontakt* prev = NULL;
    	struct kontakt* current = K_koka;
	    while (current && current->numri_ID == x[i])
	    {
	        prev = current;
	        current = current->next;
	        K_koka = current;
	        delete prev;
	    }
	
	    while (current)
	    {
	        if (current->numri_ID ==  x[i])
	        {
	            prev->next = current->next;
	            delete current;
	            current = prev->next;
	        }
	        else
	        {
	            prev = current;
	            current = current->next;
	        }
	    }
		i++;
		
	}
	
	return K_koka;
}



void redaktoKontakt (struct kontakt* K_head, int x) 
{
	int i=1;
	char c[20];
	while (i<x) {
		K_head = K_head->next;
		i++;
	}
	printf("%s %s\n", K_head->emri, K_head->mbiemri);
	afishoEmail (K_head->email);
	afishoNrKontakti (K_head->numer_kontakti);
	printf("\nEmri i ri: ");
	scanf("%s", K_head->emri);
	printf("Mbiemri i ri: ");
	scanf("%s", K_head->mbiemri);
  	printf("Email: ");
	scanf("%s", c);

}
